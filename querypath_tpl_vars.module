<?php
/**
 * @file
 * querypath_tpl_vars module. Handles allround aktivdb functionalities.
 *
 * @author Mikkel Jakobsen <mikkel@adapt.dk>
 */

/**
 * Hook to retreive module settings.
 */
define('QUERY_PATH_TPL_VARS_HOOK_SETTINGS', 'querypath_tpl_vars_settings');

/**
 * Implements MODULE_NAME_preprocess_page.
 */
function querypath_tpl_vars_preprocess_page(&$vars) {
  _querypath_tpl_vars_populate_vars($vars, 'page');
}

/**
 * Fetches html with optinal processing.
 *
 * @param array $settings
 *   Settings specified by module hooks.
 * @param string $cid
 *   Drupal cache id.
 *
 * @return string
 *   HTML.
 */
function _querypath_tpl_vars_fetch_html($settings, $cid = '') {
  if (!empty($cid) && $cache = cache_get($cid)) {
    $html = $cache->data;
  }
  else {
    $qp = htmlqp($settings['domain'])->find($settings['selector']);
    if (!empty($settings['process'])) {
      foreach ($settings['process'] as $type => $data) {
        switch ($type) {
          case 'paths_rel2abs':
            $exclude = !empty($data['exclude']) ? $data['exclude'] : array();
            _querypath_tpl_vars_rel2abs($qp, 'a', 'href', $exclude, $settings);
            _querypath_tpl_vars_rel2abs($qp, 'img', 'src', $exclude, $settings);
            _querypath_tpl_vars_rel2abs($qp, 'form', 'action', $exclude, $settings);
            break;

          case 'remove_elements':
            foreach ($data as $selector) {
              $qp->find($selector)->remove()->top($settings['selector']);
            }
            break;

        }
      }
    }
    $html = $qp->top($settings['selector'])->html();
  }

  if (!empty($html)) {
    cache_set($cid, $html, 'cache', CACHE_TEMPORARY);
    return $html;
  }
  return '';
}

/**
 * Populates template vars from hook settings.
 *
 * @param array $vars
 *   Theme vars.
 * @param string $template
 *   Which template. Ex.: 'page'.
 * @param string $prefix
 *   Variable name prefix.
 *
 * @return void
 *   Nothing to return.
 */
function _querypath_tpl_vars_populate_vars(&$vars, $template, $prefix = 'qtv') {
  $module_settings = module_invoke_all(QUERY_PATH_TPL_VARS_HOOK_SETTINGS);

  foreach ($module_settings as $var_name => $settings) {
    if (empty($settings['templates'])) {
      return;
    }
    if (!in_array($template, $settings['templates'])) {
      return;
    }

    $cache_key = '';
    if (!empty($settings['cache']) && $settings['cache'] === TRUE) {
      $cache_key = $var_name;
    }
    $vars[$prefix . '_' . $var_name] = _querypath_tpl_vars_fetch_html(
      $settings,
      $cache_key
    );
  }

}

/**
 * Converts paths from relative to absolute.
 *
 * @param object $qp
 *   Querypath object.
 * @param string $tag
 *   HTML tag.
 * @param string $attr_name
 *   Attribute name of HTML tag.
 * @param array $exclude
 *   Array of paths to exclude from processing.
 * @param array $settings
 *   Settings used for processing.
 *
 * @return void
 *   Nothing to return.
 */
function _querypath_tpl_vars_rel2abs(&$qp, $tag, $attr_name, $exclude, $settings) {
  $elements = $qp->find($tag);
  if ($elements->length >= 1) {
    foreach ($elements as $element) {
      $attr = $element->attr($attr_name);
      if (!empty($exclude[$attr_name]) && in_array($attr, $exclude[$attr_name])) {
        continue;
      }
      $element->attr($attr_name, $settings['domain'] . $element->attr($attr_name));
    }
  }
  $qp->top($settings['selector']);
}
