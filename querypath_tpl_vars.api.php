<?php
/**
 * @file
 * Hooks for the querypath_tpl_vars module.
 *
 * @author Mikkel Jakobsen <mikkel@adapt.dk>
 */

/**
 * This hook makes it possible for various modules to populate theme variables.
 */
function hook_querypath_tpl_vars_settings() {
  return array(
    'header' => array(
      // By now only page is supported.
      'templates' => array('page'),
      'domain' => 'http://acme.com',
      'selector' => '#header',
      'cache' => TRUE,
      'process' => array(
        'paths_rel2abs' => array(),
      ),
    ),
    'navigation' => array(
      // By now only page is supported.
      'templates' => array('page'),
      'domain' => 'http://acme.com',
      'selector' => '#navigation',
      'cache' => TRUE,
      'process' => array(
        'remove_elements' => array('.region-navigation'),
        'paths_rel2abs' => array(
          'exclude' => array(
            'href' => array('/aktiviteter'),
          ),
        ),
      ),
    ),
  );
}
